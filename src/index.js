const axios = require('axios');

let AfghanistanAudio = "http://www.navyband.navy.mil/anthems/anthems/afghanistan.mp3";
let AlbaniaAudio = "http://www.navyband.navy.mil/anthems/anthems/albania.mp3";
let AlgeriaAudio = "http://www.navyband.navy.mil/anthems/anthems/algeria.mp3";
let AngolaAudio = "http://www.navyband.navy.mil/anthems/anthems/angola.mp3";
let ArgentinaAudio = "http://www.navyband.navy.mil/anthems/anthems/argentina%20(long).mp3";
let ArmeniaAudio = "http://www.navyband.navy.mil/anthems/anthems/armenia.mp3";
let AustraliaAudio = "http://www.navyband.navy.mil/anthems/anthems/australia%20(advance%20australia%20fair).mp3"
let AustriaAudio = "http://www.navyband.navy.mil/anthems/anthems/austria.mp3";
let BahamasAudio = "http://www.navyband.navy.mil/anthems/anthems/bahamas.mp3";
let BangladeshAudio = "http://www.navyband.navy.mil/anthems/anthems/bangladesh.mp3";



let body = document.querySelector('body');

let score = 0;

document.querySelector('.score').innerHTML = `${score}`;






axios.get('https://restcountries.eu/rest/v2/all')
    .then(function (response) {
        let body = document.querySelector('body');
        let vraiPays = Math.floor(Math.random() * 11);


        let ObjectsTab = [{
            countryName: response.data[0].name,
            countryFlag: response.data[0].flag,
            countryAnthem: AfghanistanAudio
        },
        {
            countryName: response.data[2].name,
            countryFlag: response.data[2].flag,
            countryAnthem: AlbaniaAudio
        },
        {
            countryName: response.data[3].name,
            countryFlag: response.data[3].flag,
            countryAnthem: AlgeriaAudio
        },
        {
            countryName: response.data[6].name,
            countryFlag: response.data[6].flag,
            countryAnthem: AngolaAudio
        },
        {
            countryName: response.data[10].name,
            countryFlag: response.data[10].flag,
            countryAnthem: ArgentinaAudio
        },
        {
            countryName: response.data[11].name,
            countryFlag: response.data[11].flag,
            countryAnthem: ArmeniaAudio
        },
        {
            countryName: response.data[13].name,
            countryFlag: response.data[13].flag,
            countryAnthem: AustraliaAudio
        },
        {
            countryName: response.data[14].name,
            countryFlag: response.data[14].flag,
            countryAnthem: AustriaAudio
        },
        {
            countryName: response.data[16].name,
            countryFlag: response.data[16].flag,
            countryAnthem: BahamasAudio
        },
        {
            countryName: response.data[18].name,
            countryFlag: response.data[18].flag,
            countryAnthem: BangladeshAudio
        }]
        return ObjectsTab;




        // http://www.navyband.navy.mil/anthems/anthems/afghanistan.mp3

    }).then((ObjectsTab) => {
        let score = 0;
        let result = '';

        document.querySelector('button.ready').addEventListener('click', () => {
            let compteur = 30;
            setInterval(() => {
                if (compteur === 0) {
                    compteur = 30;
                }
                if (result === 'correct') {
                    result = '';
                    score++;
                    compteur = 30;

                }
                if (result === 'faux') {
                    result = '';
                    compteur = 30;
                }
                if (compteur === 30) {
                    // alert(`${ObjectsTab[vraiPays].countryName}`);

                    startGame(ObjectsTab, score);
                }
                compteur--;
                document.querySelector('.compteur').innerHTML = `${compteur}`;
            }, 1000);
        });

    }).catch(function (error) {
        console.log(error);
    });



function startGame(ObjectsTab, score) {
    document.querySelector('.score').innerHTML = `${score}`;

    let vraiPays = Math.floor((Math.random() * 9));
    let randomTruc = Math.floor((Math.random() * 3) + 1);
    let savFauxPays = 'truc';
    let x = 1;
    while (x <= 3) {
        let fauxPays = Math.floor(Math.random() * 9);
        let card = document.querySelector(`.card${x}`);
        let cardText = document.querySelector(`.card-text${x}`);
        if (randomTruc === x) {
            cardText.innerHTML = `${ObjectsTab[vraiPays].countryName}`;
            card.style.backgroundImage = `url("${ObjectsTab[vraiPays].countryFlag}")`;
            card.style.backgroundSize = "cover";
            card.style.height = "200px";
        }
        else {
            if (fauxPays === vraiPays && fauxPays !== 9) {
                fauxPays++;
                console.log('fauxPays est égale à vraiPays');
            }
            else if (fauxPays === 9) {
                fauxPays--;
                console.log('fauxPays est égale a vraiPays et égale 10');
            }
            if (fauxPays === savFauxPays) {
                fauxPays++;
                cardText.innerHTML = `${ObjectsTab[fauxPays].countryName}`;
                card.style.backgroundImage = `url("${ObjectsTab[fauxPays].countryFlag}")`;
                card.style.backgroundSize = "cover";
                card.style.height = "200px";
            } else {
                cardText.innerHTML = `${ObjectsTab[fauxPays].countryName}`;
                card.style.backgroundImage = `url("${ObjectsTab[fauxPays].countryFlag}")`;
                card.style.backgroundSize = "cover";
                card.style.height = "200px";

            }
            savFauxPays = fauxPays;

        }
        x++;
    }

    let audio = document.querySelector("audio");
    audio.src = ObjectsTab[vraiPays].countryAnthem;
}

